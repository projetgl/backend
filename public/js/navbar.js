$(document).ready(function(){
	/*  Foundation Init    */
	$(document).foundation();

	/*    Mean navigation menu scroll to    */
    $('#mean_nav ul li a').click(function(e){
    	scrollTo($(this).attr('href'), 900, 'easeInOutCubic');
    });
});