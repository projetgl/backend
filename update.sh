#!/usr/bin/env bash
git pull
composer install --optimize-autoloader
#yarn
#yarn production
php artisan migrate --force
php artisan l5-swagger:generate

# Optimizations for production
php artisan config:cache
php artisan route:cache

# Queue workers
sudo supervisorctl restart isend-worker:*
