@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-sign-up.css') }}"/>
@endpush

@section('title')
    Inscription
@endsection

@section('content')
    <div class="site-content">
        <div class="box" >
            <form id="form-sign-up" name="form" method="post">
                {{ csrf_field() }}

                <p class="libelle">Nom</p>
                <input class="input" type="text" name="last_name" value="{{ old('last_name') }}" required />
                <p class="libelle">Prénom</p>
                <input class="input" type="text" name="first_name" value="{{ old('first_name') }}" required />
                <p class="libelle">Pseudonyme</p>
                <input class="input" type="text" name="nickname" value="{{ old('nickname') }}" required />
                <p class="libelle">Adresse Mail</p>
                <input class="input" type="text" name="email" value="{{ old('email') }}" required />
                <p class="libelle">Mot de passe</p>
                <input class="input" type="password" name="password" required />
                <p class="libelle">Confirmation du mot de passe</p>
                <input class="input" type="password" name="password_confirmation" required />
                <p class="libelle">Adresse</p>
                <input class="input" type="text" name="address_line1" value="{{ old('address_line1') }}"/>
                <p class="libelle">Adresse (Ligne 2)</p>
                <input class="input" type="text" name="address_line2" value="{{ old('address_line2') }}"/>
                <p class="libelle">Code Postal</p>
                <input class="input" type="text" name="zipcode" value="{{ old('zipcode') }}"/>
                <p class="libelle">Ville</p>
                <input class="input" type="text" name="city" value="{{ old('city') }}"/>
                <p class="libelle">Pays</p>
                <input class="input" type="text" name="country" value="{{ old('country') or 'France' }}"/>
                <div id="cmd_box">
                    <input id="input_submit" type="submit" name="login" value="S'inscrire"/>
                </div>
            </form>
        </div>
    </div>
@endsection
