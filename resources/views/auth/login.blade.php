@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-sign-in.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style-google-fb.css') }}"/>
@endpush

@section('title')
    Connexion
@endsection

@section('content')
    <div class="box">
        <form id="form-sign-in" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

            <input class="input" type="text" name="email"
                   value="{{ old('email') }}"
                   required autofocus placeholder="Email"/>

            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

            <input class="input" type="password" name="password" required placeholder="Mot de passe"/>

            @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif

            <label>
                <input type="checkbox"
                       name="remember" {{ old('remember') ? 'checked' : '' }}> Se souvenir de moi
            </label>

            <input class="input_yellow" type="submit" name="login" value="Connexion"/>
        </form>

        {{--<button class="loginBtn loginBtn--facebook">Se connecter avec Facebook</button>--}}
        {{--<button class="loginBtn loginBtn--google">Se connecter avec Google</button>--}}

        <p>
            <a id="find-psw" href="{{ url('/password/reset') }}">Mot de passe oublié</a>
        </p>
    </div>
@endsection
