@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-find-psw.css') }}"/>
@endpush

@section('title')
    Changement de mot de passe
@endsection

@section('content')
    <div class="box">
        <h2>Changement de mot de passe</h2>

        <form method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">

            <p class="libelle">
                Saisissez l'adresse e-mail associée à votre compte ainsi que votre nouveau mot de passe.
            </p>

            <input class="input_black" type="email" name="email"
                   value="{{ $email or old('email') }}"
                   placeholder="Adresse mail"
                   required autofocus/>

            <input id="password" type="password" class="input_black" name="password" required
                   placeholder="Nouveau mot de passe">

            <input id="password-confirm" type="password" class="input_black"
                   name="password_confirmation" required placeholder="Nouveau mot de passe (confirmation)">

            <input class="find" type="submit" name="submit" value="Modifier le mot de passe">
        </form>
    </div>
@endsection
