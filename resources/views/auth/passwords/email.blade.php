@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-find-psw.css') }}"/>
@endpush

@section('title')
    Mot de passe oublié
@endsection

@section('content')
    <div class="box">
        <h2>Mot de passe oublié ?</h2>

        <form method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <p class="libelle">
                Saisissez l'adresse e-mail associée à votre compte pour recevoir un e-mail de réinitialisation de
                votre mot de passe.
            </p>

            <input class="input_black" type="email" name="email" value="{{ old('email') }}"
                   required placeholder="Adresse mail"/>

            <input class="find" type="submit" name="submit" value="Récupérer le mot de passe">
        </form>
    </div>
@endsection
