<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carte postale pour {{ $postcard->recipient }}</title>

    <style>
        /* http://meyerweb.com/eric/tools/css/reset/
           v2.0 | 20110126
           License: none (public domain)
        */

        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed,
        figure, figcaption, footer, header, hgroup,
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
        }

        /* HTML5 display-role reset for older browsers */
        article, aside, details, figcaption, figure,
        footer, header, hgroup, menu, nav, section {
            display: block;
        }

        body {
            line-height: 1;
        }

        ol, ul {
            list-style: none;
        }

        blockquote, q {
            quotes: none;
        }

        blockquote:before, blockquote:after,
        q:before, q:after {
            content: '';
            content: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
    </style>

    <style>
        * * {
            box-sizing: border-box;
        }

        img {
            width: 420px;
            height: 298px;
        }

        .page-break {
            page-break-after: always;
        }

        .backside {
            width: 420px;
            height: 298px;
            position: relative;
            font-family: sans-serif;
            font-size: 12px;
            background: url("{{ asset('img/postcard_backside.png') }}") no-repeat;
            background-size: 420px 298px;
        }

        .body {
            position: absolute;
            font-size: 16px;
            top: 40px;
            left: 10px;
            width: 190px;
            height: 13rem;
            word-wrap: break-word;
            overflow: hidden;
        }

        ul {
            list-style-type: none;
        }

        li {
            position: absolute;
            vertical-align: top;
            text-align: right;
            right: 35px;
        }

        .recipient {
            top: 110px;
        }

        .address_line1 {
            top: 140px;
        }

        .address_line2 {
            top: 170px;
        }

        .zipcode_city {
            top: 200px;
        }

        .country {
            top: 230px;
        }
    </style>
</head>
<body>
<img src="{{ $postcard->getPublicUrl() }}" alt="" width="420" height="298">

<div class="page-break"></div>

<div class="backside">
    <p class="body">
        {{ nl2br($postcard->body) }}
    </p>

    <ul>
        <li class="recipient">{{ $postcard->recipient }}</li>
        <li class="address_line1">{{ $postcard->address_line1 }}</li>
        <li class="address_line2">{{ $postcard->address_line2 }}</li>
        <li class="zipcode_city">{{ $postcard->zipcode }} {{ $postcard->city }}</li>
        <li class="country">{{ $postcard->country }}</li>
    </ul>
</div>
</body>
</html>
