@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-index.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style-dropzone.css') }}"/>
@endpush

@push('js')
    <script src="{{ asset('js/dropzone.js') }}" type="text/javascript"></script>
@endpush

@section('title')
    Accueil
@endsection

@section('content')
    <div id="box">
        <div id="box-form">
            <form id="form-recherche" method="get" action="{{ route('searchPicture') }}">
                <input id="search_box" type="text" name="tag" placeholder="Rechercher un lieu"/>

                <button type="submit" id="search">Rechercher</button>
            </form>
        </div>
    </div>
@endsection
