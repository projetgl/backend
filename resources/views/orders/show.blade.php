@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-mes-commandes.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style-tables.css') }}"/>
@endpush

@section('title')
    Détail de la commande : {{ $order->id }}
@endsection

@section('content')
    <div class="container">
        <h1>Détail commande n°{{ $order->id }}</h1>
        <p>
            N° de commande : <span>{{ $order->id }}</span>
            <br>
            Date de la commande : {{ $order->created_at }}
            <br>
            Montant de la commande : {{ $order->price }}
            <br>
            Mode de règlement : CB en ligne
            <br>
            Etat de votre commande : {{ $order->status }}
            <br>
            Adresse de livraison :
            {{ $order->recipient }} <br>
            {{ $order->address_line1 }} <br>
            {{ $order->address_line2 }} <br>
            {{ $order->zipcode }} {{ $order->city }}<br>
            {{ $order->country }}
            <br>
            <br>
            Adresse de facturation : <br>
            {{ $user->first_name }} {{ $user->last_name }} <br>
            {{ $user->address_line1 }} <br>
            {{ $user->address_line2 }} <br>
            {{ $user->zipcode }} {{ $user->city }}<br>
            {{ $user->country }} <br>
        </p>

        <table class="table_total">
            <tr>
                <td class="td_total">Total produit TTC</td>
                <td class="td_total">{{ ($order->price-200)/100 }} €</td>
            </tr>
            <tr>
                <td class="td_total">Frais de service</td>
                <td class="td_total">2.00 €</td>
            </tr>
            <tr>
                <td class="td_total">Total</td>
                <td class="td_total">{{ ($order->price)/100 }} €</td>
            </tr>
        </table>

        <a href="{{ route('showOrderBill', $order) }}">
            <button class="btn-block">Imprimer la facture</button>
        </a>
    </div>
@endsection
