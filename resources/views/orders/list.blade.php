@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-mes-commandes.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style-tables.css') }}"/>
@endpush

@section('title')
    Mes commandes
@endsection

@section('content')
    <div class="container">
        <table>
            <caption>Mes commandes</caption>

            <thead>
            <tr>
                <th scope="col">N° Commande</th>
                <th scope="col">Date</th>
                <th scope="col">État</th>
                <th scope="col">Total TTC (€)</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>

            <tbody>
            @forelse($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>{{ $order->status }}</td>
                    <td>{{ $order->price }}</td>
                    <td>
                        <a href="{{ route('showOrder', $order) }}">Voir le détail</a> <br>
                        <a href="{{ route('showOrderBill', $order) }}">Imprimer la facture</a>
                    </td>
                </tr>
            @empty
                <td colspan="6">Pas de commande</td>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
