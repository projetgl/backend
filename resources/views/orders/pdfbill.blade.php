<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Facture de la commande n°{{ $order->id }}</title>
</head>
<body>
<h1>Détail commande n°{{ $order->id }}</h1>
<p>
    N° de commande : <span>{{ $order->id }}</span>
    <br>
    Date de la commande : {{ $order->created_at }}
    <br>
    Montant de la commande : {{ $order->price }}
    <br>
    Mode de règlement : CB en ligne
    <br>
    Etat de votre commande : {{ $order->status }}
    <br>
    Adresse de livraison :
    {{ $order->recipient }} <br>
    {{ $order->address_line1 }} <br>
    {{ $order->address_line2 }} <br>
    {{ $order->zipcode }} {{ $order->city }}<br>
    {{ $order->country }}
    <br>
    <br>
    Adresse de facturation : <br>
    {{ $user->first_name }} {{ $user->last_name }} <br>
    {{ $user->address_line1 }} <br>
    {{ $user->address_line2 }} <br>
    {{ $user->zipcode }} {{ $user->city }}<br>
    {{ $user->country }} <br>
</p>

<table>
    <tr>
        <td>Total produit TTC</td>
        <td>{{ ($order->price-200)/100 }} €</td>
    </tr>
    <tr>
        <td>Frais de service</td>
        <td>2.00 €</td>
    </tr>
    <tr>
        <td>Total</td>
        <td>{{ ($order->price)/100 }} €</td>
    </tr>
</table>
</body>
</html>