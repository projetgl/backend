@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-profil.css') }}"/>
@endpush

@section('title')
    Profil
@endsection

@section('content')
    <div class="box">
        <form enctype="multipart/form-data" class="login-form" name="form" method="post"
              action="{{ route('updateProfile') }}">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}

            <img src="{{ $profile_picture or asset('img/profil-picture.jpg') }}" class="ProfilPicture"/>
            <div>
                <p class="left">Nom :</p>
                <input type="text" name="last_name" placeholder="Nom"
                       value="{{ old('last_name') ?? $user->last_name }}"/>
            </div>
            <div>
                <p class="left">Prénom :</p>
                <input type="text" name="first_name" placeholder="Prénom"
                       value="{{ old('first_name') ?? $user->first_name }}"/>
            </div>
            {{--<div>--}}
            {{--<p class="left">Mot de passe :</p>--}}
            {{--<input type="password" name="password" placeholder="Mot de passe"/>--}}
            {{--</div>--}}
            {{--<div>--}}
            {{--<p class="left">Mot de passe (confirmation) :</p>--}}
            {{--<input type="password" name="password_confirmation" placeholder="Mot de passe (confirmation)"/>--}}
            {{--</div>--}}
            <div>
                <p class="left">Email :</p>
                <input type="email" name="email" placeholder="Email" value="{{ old('email') ?? $user->email }}"/>
            </div>

            <div>
                <p class="left">Adresse (ligne 1) :</p>
                <input type="text" name="address_line1" placeholder="Adresse (ligne 1)"
                       value="{{ old('address_line1') ?? $user->address_line1 }}"/>
            </div>

            <div>
                <p class="left">Adresse (ligne 2) :</p>
                <input type="text" name="address_line2" placeholder="Adresse (ligne 2)"
                       value="{{ old('address_line2') ?? $user->address_line2 }}"/>
            </div>

            <div>
                <p class="left">Code postal :</p>
                <input type="number" name="zipcode" placeholder="Code postal"
                       value="{{ old('zipcode') ?? $user->zipcode }}"/>
            </div>

            <div>
                <p class="left">Ville :</p>
                <input type="text" name="city" placeholder="Ville"
                       value="{{ old('city') ?? $user->city }}"/>
            </div>

            <div>
                <p class="left">Pays :</p>
                <input type="text" name="country" placeholder="Pays"
                       value="{{ old('country') ?? $user->country }}"/>
            </div>

            <div>
                <p class="left">Photo de profil :</p>
                <input type="file" name="profile_picture"/>
            </div>

            <input class="btn_submit separator" type="submit" value="Mettre à jour"/>
        </form>

        <form enctype="multipart/form-data" class="login-form" name="form" method="post"
              action="{{ route('deleteProfile') }}">
            <input type="hidden" name="_method" value="DELETE">
            {{ csrf_field() }}

            <input class="btn-delete" type="submit" value="Supprimer le compte"/>
        </form>

        <form class="login-form" name="form" method="post" action="{{ route('updatePassword') }}">
            {{ csrf_field() }}

            <div>
                <p class="left">Mot de passe :</p>
                <input type="password" name="password" placeholder="Mot de passe"/>
            </div>

            <div>
                <p class="left">Mot de passe (confirmation) :</p>
                <input type="password" name="password_confirmation" placeholder="Mot de passe (confirmation)"/>
            </div>

            <input class="btn_submit separator" type="submit" value="Mettre à jour le mot de passe"/>
        </form>
    </div>
@endsection
