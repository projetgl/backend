@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-about.css') }}"/>
@endpush

@section('title')
    À propos
@endsection

@section('content')
    <div class="box">
        <h1 id="titre">Mentions Légales</h1>
        <p>
            Altera sententia est, quae definit amicitiam paribus officiis ac voluntatibus. Hoc quidem est nimis exigue
            et exiliter ad calculos vocare amicitiam, ut par sit ratio acceptorum et datorum. Divitior mihi et
            affluentior videtur esse vera amicitia nec observare restricte, ne plus reddat quam acceperit; neque enim
            verendum est, ne quid excidat, aut ne quid in terram defluat, aut ne plus aequo quid in amicitiam
            congeratur.
        </p>
        <p>
            Quis enim aut eum diligat quem metuat, aut eum a quo se metui putet? Coluntur tamen simulatione dumtaxat ad
            tempus. Quod si forte, ut fit plerumque, ceciderunt, tum intellegitur quam fuerint inopes amicorum. Quod
            Tarquinium dixisse ferunt, tum exsulantem se intellexisse quos fidos amicos habuisset, quos infidos, cum iam
            neutris gratiam referre posset.
        </p>
        <p>
            Et quoniam mirari posse quosdam peregrinos existimo haec lecturos forsitan, si contigerit, quamobrem cum
            oratio ad ea monstranda deflexerit quae Romae gererentur, nihil praeter seditiones narratur et tabernas et
            vilitates harum similis alias, summatim causas perstringam nusquam a veritate sponte propria digressurus.
        </p>
        <p>
            Cognitis enim pilatorum caesorumque funeribus nemo deinde ad has stationes appulit navem, sed ut Scironis
            praerupta letalia declinantes litoribus Cypriis contigui navigabant, quae Isauriae scopulis sunt
            controversa.
        </p>
        <p>
            Ergo ego senator inimicus, si ita vultis, homini, amicus esse, sicut semper fui, rei publicae debeo. Quid?
            si ipsas inimicitias, depono rei publicae causa, quis me tandem iure reprehendet, praesertim cum ego omnium
            meorum consiliorum atque factorum exempla semper ex summorum hominum consiliis atque factis mihi censuerim
            petenda.
        </p>
    </div>
@endsection
