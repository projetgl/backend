@extends('layouts.app')

@section('title')
    Administration
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-administration.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style-tables.css') }}">
@endpush

@section('content')
    <div class="container">
        <table>
            <caption>Commandes</caption>

            <thead>
            <tr>
                <th scope="col">N° Commande</th>
                <th scope="col">Date</th>
                <th scope="col">État</th>
                <th scope="col">Total TTC (€)</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>

            <tbody>
            @forelse($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>{{ $order->status }}</td>
                    <td>{{ $order->price }}</td>
                    <td>
                        <a href="{{ route('adminShowOrder', $order) }}">
                            <button class="btn-block thin">Voir le détail</button>
                        </a>

                        @if($order->status === 'En préparation')
                            <form action="{{ route('adminUpdateOrder', $order) }}" method="POST">
                                @method('PUT')
                                @csrf

                                <input type="hidden" name="status" value="Expédié">
                                <button type="submit" class="btn-block thin">Passer en expédié</button>
                            </form>
                        @endif

                        @if($order->status === 'En préparation' || $order->status === 'Expédié' )
                            <form action="{{ route('adminUpdateOrder', $order) }}" method="POST">
                                @method('PUT')
                                @csrf

                                <input type="hidden" name="status" value="Reçu">
                                <button type="submit" class="btn-block thin">Passer en livré</button>
                            </form>
                        @endif
                    </td>
                </tr>
            @empty
                <td colspan="6">Pas de commande</td>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
