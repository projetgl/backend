@extends('layouts.app')

@section('title')
    Administration : Liste des utilisateurs
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-administration.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style-tables.css') }}"/>
@endpush

@section('content')
    <div class="container">
        <table>
            <caption>Utilisateurs</caption>

            <thead>
            <tr>
                <th scope="col">N° Client</th>
                <th scope="col">Date d'inscription</th>
                <th scope="col">Confirmé</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Email</th>
            </tr>
            </thead>

            <tbody>
            @forelse($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->confirmed ? 'Oui' : 'Non' }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->email }}</td>
                </tr>
            @empty
                <td colspan="6">Pas d'utilisateurs</td>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
