@extends('layouts.app')

@section('title')
    Administration
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-administration.css') }}">
@endpush

@section('content')
    <div class="container">
        <h1>Administration</h1>

        <h3>Utilisateurs</h3>
        <ul>
            <li>Utilisateurs confirmés : {{ $users['confirmed'] }}</li>
            <li>Utilisateurs non confirmés : {{ $users['notConfirmed'] }}</li>
            <li>Total des utilisateurs : {{ $users['total'] }}</li>
        </ul>

        <div>
            <a href="{{ route('adminShowUsers') }}">
                <button class="btn-block">
                    Voir les utilisateurs
                </button>
            </a>
        </div>

        <h3>Commandes</h3>
        <ul>
            <li>Commandes en préparation : {{ $orders['pending'] }}</li>
            <li>Commandes envoyés : {{ $orders['sent'] }}</li>
            <li>Commandes reçues : {{ $orders['delivered'] }}</li>
            <li>Total des commandes : {{ $orders['total'] }}</li>
        </ul>

        <div>
            <a href="{{ route('adminAllOrders') }}">
                <button class="btn-block">
                    Voir les commandes
                </button>
            </a>
        </div>
    </div>
@endsection
