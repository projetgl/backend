<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>iSend • @yield('title')</title>
    <meta name="description" content=""/>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style-navbar.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/style-global.css') }}"/>
    @stack('css')
</head>
<body>
<header>
    <div class="row">
        <nav class="top-bar" data-topbar role="navigation" data-options="is_hover: false"
             data-options="dropdown_autoclose: false">
            <ul class="title-area">
                <li class="name">
                    <a href="{{ url('/') }}" id="logo-navbar">
                    </a>
                </li>
                <span class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></span>
            </ul>
            <section class="top-bar-section" id="mean_nav">
                <ul class="right">
                    @guest
                        <li><a href="{{ route('register') }}">Inscription</a></li>
                        <li><a href="{{ route('login') }}">Connexion</a></li>
                    @else
                        @if(Auth::user()->is_admin)
                            <li><a href="{{ route('adminIndex') }}">Administration</a></li>
                        @endif
                        <li><a href="{{ route('showAddPicture') }}">Ajouter une photo</a></li>
                        <li><a href="{{ route('profile') }}">Mon compte</a></li>
                        <li><a href="{{ route('showAllOrders') }}">Mes commandes</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Déconnexion
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endguest

                    <li><a href="{{ route('about') }}">À propos</a></li>
                </ul>
            </section>
        </nav>
    </div>
</header>

@if(count($errors) > 0)
    <ul style="color: red">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

<div class="site-content">
    @yield('content')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/foundation.min.js') }}"></script>
<script src="{{ asset('js/navbar.js') }}" type="text/javascript"></script>
@stack('js')
</body>
</html>
