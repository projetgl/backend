@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-about.css') }}"/>
    <style>
        .pictures {
            display: flex;
            flex-wrap: wrap;
        }

        figure {
            max-width: 15rem;
        }

        figure img {
            max-width: 15rem;
            height: auto;
        }

        figure figcaption {
            text-align: center;
        }
    </style>
@endpush

@section('title')
    Résultat{{ $results > 1 ? 's' : '' }} pour {{ $tag }}
@endsection

@section('content')
    <div class="box">
        <h1>{{ $results }} résultat{{ $results > 1 ? 's' : '' }} pour {{ $tag }}</h1>

        <div class="pictures">
            @forelse ($pictures as $pic)
                <a href="{{ route('showPicture', $pic) }}">
                    <figure>
                        <img src="{{ $pic->getPublicUrl() }}" alt="{{ $pic->title }}">

                        <figcaption>{{ $pic->title }}</figcaption>
                    </figure>
                </a>
            @empty
                <p>Aucun résultat</p>
            @endforelse
        </div>
    </div>
@endsection
