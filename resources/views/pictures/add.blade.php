@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-profil.css') }}"/>
@endpush

@section('title')
    Ajouter une photo
@endsection

@section('content')
    <div class="box">
        <form enctype="multipart/form-data" class="login-form" name="form" method="post"
              action="{{ route('addPicture') }}">
            {{ csrf_field() }}

            <div>
                <p class="left">Titre :</p>
                <input type="text" name="title" placeholder="Titre"
                       value="{{ old('title') }}"/>
            </div>

            <div>
                <p class="left">Description :</p>
                <input type="text" name="description" placeholder="Description"
                       value="{{ old('description') }}"/>
            </div>

            <div>
                <p class="left">Prix (en centimes) :</p>
                <input type="number" name="price" placeholder="Prix"
                       value="{{ old('price') }}"/>
            </div>

            <div>
                <p class="left">Photo :</p>
                <input type="file" name="picture"/>
            </div>

            <input type="hidden" name="lat" id="lat" value="0">
            <input type="hidden" name="lng" id="lng" value="0">

            <input class="btn_submit separator" type="submit" value="Envoyer la photo"/>
        </form>
    </div>
@endsection

@push('js')
    <script>
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (position) {
            document.getElementById('lat').value = position.coords.latitude;
            document.getElementById('lng').value = position.coords.longitude;
          });
        }
    </script>
@endpush
