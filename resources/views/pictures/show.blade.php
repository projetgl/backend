@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-about.css') }}"/>
    <style>
        .Box_share {
            display: flex;
            justify-content: space-around;
            width: 80%;
            margin: auto;
        }

        @media (min-width: 768px) {
            .Box_share {
                width: 50%;
            }
        }

        .Box_share input[type=image], .icon {
            max-width: 2rem;
            height: auto;
            background: none;
        }

        .picture {
            margin-bottom: 1.5rem;
        }

        .picture img {
            display: block;
            height: auto;
            margin: auto;
        }
    </style>
@endpush

@section('title')
    Photo : {{ $picture->title }}
@endsection

@section('content')
    <div class="box">
        <div class="picture">
            <img src="{{ $picture_url }}" alt="{{ $picture->title }}" class="picture">
        </div>

        <div class="Box_share">
            @can('like', App\Picture::class)
                <form action="{{ route('likePicture', $picture) }}" method="post">
                    @method('POST')
                    @csrf
                    <input class="icon" type="image" src="{{ asset('img/ico_up.png') }}">
                </form>

                <form action="{{ route('dislikePicture', $picture) }}" method="post">
                    @method('POST')
                    @csrf
                    <input class="icon" type="image" src="{{ asset('img/ico_down.png') }}">
                </form>
            @endcan

            <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" target="_blank">
                <img class="icon" src="{{ asset('img/share.png') }}">
            </a>

            <a href="{{ route('downloadPicture', $picture) }}">
                <img class="icon" src="{{ asset('img/dl.png') }}">
            </a>

            @can('delete', $picture)
                <form action="{{ route('deletePicture', $picture) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <input class="icon" type="image" src="{{ asset('img/trash.png') }}">
                </form>
            @endcan
        </div>

        <h3>{{ $picture->title }}</h3>

        <p>{{ nl2br($picture->description) }}</p>

        <h3>Tags</h3>

        <ul>
            @foreach($tags as $tag)
                <li>{{ $tag->label }}</li>
            @endforeach
        </ul>

        @can('order', $picture)
            <a href="{{ route('showOrderPicture', $picture) }}">
                <button class="btn-block">Commander</button>
            </a>
        @endcan

        @can('update', $picture)
            <a href="{{ route('showEditPicture', $picture) }}">
                <button class="btn-block">Éditer</button>
            </a>
        @endcan

        <ul>
            <li><strong>Utilisateur :</strong> {{ $picture->owner->nickname }}</li>
            @if ($picture->lat !== 0.0 && $picture->lng !== 0.0)
                <li>
                    <strong>Coordonnées :</strong>
                    <a href="https://google.com/maps?q={{ $picture->lat }},{{ $picture->lng }}">
                        {{ $picture->lat }},{{ $picture->lng }}
                    </a>
                </li>
            @endif
            <li><strong>Prix :</strong> {{ $picture->price/100 }} €</li>
            <li><strong>Score :</strong> {{ $picture->score }}</li>
        </ul>
    </div>
@endsection
