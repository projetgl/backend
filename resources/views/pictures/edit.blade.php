@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-profil.css') }}"/>
@endpush

@section('title')
    Éditer la photo : {{ $picture->title }}
@endsection

@section('content')
    <div class="box">
        <form enctype="multipart/form-data" class="login-form" name="form" method="post"
              action="{{ route('editPicture', $picture) }}">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}

            <div>
                <p class="left">Titre :</p>
                <input type="text" placeholder="Titre" value="{{ $picture->title }}" disabled/>
            </div>

            <div>
                <p class="left">Description :</p>
                <input type="text" name="description" placeholder="Description"
                       value="{{ old('description') ?? $picture->description }}"/>
            </div>

            <div>
                <p class="left">Prix (en centimes) :</p>
                <input type="number" name="price" placeholder="Prix" value="{{ old('price') ?? $picture->price-200 }}"/>
            </div>

            <div>
                <p class="left">Tags (séparés par des points virgules) :</p>
                <input type="text" name="tags" placeholder="Tags" value="{{ old('tags') ?? $tags }}"/>
            </div>

            <input class="btn_submit separator" type="submit" value="Envoyer la photo"/>
        </form>
    </div>
@endsection
