@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style-personnalisation.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/w3.css') }}"/>
    <style>
        form {
            margin-bottom: 2rem;
        }

        textarea {
            margin-bottom: 1rem;
        }
    </style>
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/camanjs/4.1.2/caman.full.min.js"></script>
    <script src="{{ asset('js/personnalisation.js') }}"></script>
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script>{{-- Passing the pic url to Javascript --}}
        window.iSend = {
        "pictureUrl": '{{ $picture_url }}',
        'picturePrice': {{ $picture->price }},
        'pictureTitle': '{{ $picture->title }}'
      };

      window.handler = StripeCheckout.configure({
        key: '{{ config('services.stripe.key') }}',
        image: '{{ asset('img/logo_transparent_black.png') }}',
        locale: 'auto',
        token: function (token) {
          $('#stripe_token').val(token.id);
          $('#orderForm').submit();
        }
      });

      window.addEventListener('popstate', function () {
        window.handler.close();
      });
    </script>
@endpush

@section('title')
    Personnalisation : {{ $picture->title }}
@endsection

@section('content')
    <div class="site-container">
        <div class="site-pusher">
            <div class="back w3-container">
                <div class="w3-half w3-container">
                    <div class="w3-half w3-container">
                        <canvas id="canvas"></canvas>
                    </div>
                </div>
                <div class="w3-half w3-container mobile">
                    <form method="post" id="orderForm" action="{{ route('orderPicture') }}">
                        {{ csrf_field() }}
                        <textarea class="text-area w3-row" placeholder="Saisissez votre texte" name="body"
                                  value="{{ old('body') }}"></textarea>
                        <input type="hidden" id="b64img" value="" name="image">
                        <input type="hidden" name="stripe_token" id="stripe_token">
                        <input type="hidden" name="picture_id" value="{{ $picture->id }}">

                        <input type="text" name="recipient" placeholder="Destinataire"
                               value="{{ old('recipient') }}" required>
                        <input type="text" name="address_line1" placeholder="Adresse (ligne 1)"
                               value="{{ old('address_line1') }}" required>
                        <input type="text" name="address_line2" placeholder="Adresse (ligne 2)"
                               value="{{ old('address_line2') }}">
                        <input type="number" name="zipcode" placeholder="Code postal"
                               value="{{ old('zipcode') }}" required>
                        <input type="text" name="city" placeholder="Ville"
                               value="{{ old('city') }}" required>
                        <input type="text" name="country" placeholder="Pays" value="France"
                               value="{{ old('country') }}" required>

                        <button type="submit" id="orderPicture" class="btn-block">Commander la carte postale</button>
                    </form>

                    <div class="box-slider w3-row">
                        <div class="slider-container">
                            <div class="slider-group">
                                <label for="hue">Hue</label>
                                <input id="hue" name="hue" type="range" min="0" max="300" value="0">
                                <label for="contrast">Contrast</label>
                                <input id="contrast" name="contrast" type="range" min="-20" max="20" value="0">
                            </div>
                            <div class="slider-group">
                                <label for="vibrance">Vibrance</label>
                                <input id="vibrance" name="vibrance" type="range" min="0" max="400" value="0">
                                <label for="sepia">Sepia</label>
                                <input id="sepia" name="sepia" type="range" min="0" max="100" value="0">
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="filters best_filters">
                    <button id="resetbtn" class="btn btn-success">Reset</button>
                    <button id="brightnessbtn" class="btn btn-primary">Brightness</button>
                    <button id="noisebtn" class="btn btn-primary">Noise</button>
                    <button id="sepiabtn" class="btn btn-primary">Sepia</button>
                    <button id="contrastbtn" class="btn btn-primary">Contrast</button>
                    <button id="colorbtn" class="btn btn-primary">Colorize</button>
                    <button id="vintagebtn" class="btn btn-primary">Vintage</button>
                </nav>

                <nav class="filters">

                    <button id="tiltshiftbtn" class="btn btn-primary">Tilt Shift</button>
                    <button id="radialblurbtn" class="btn btn-primary">Radial Blur</button>
                    <button id="edgeenhancebtn" class="btn btn-primary">Edge Enhance</button>
                    <button id="posterizebtn" class="btn btn-primary">Posterize</button>
                    <button id="claritybtn" class="btn btn-primary">Clarity</button>
                    <button id="orangepeelbtn" class="btn btn-primary">Orange Peel</button>
                    <button id="sincitybtn" class="btn btn-primary">Sin City</button>

                </nav>

                <nav class="filters">
                    <button id="hazydaysbtn" class="btn btn-primary">Hazy</button>
                    <button id="lovebtn" class="btn btn-primary">Love</button>
                    <button id="grungybtn" class="btn btn-primary">Grungy</button>
                    <button id="jarquesbtn" class="btn btn-primary">Jarques</button>
                    <button id="pinholebtn" class="btn btn-primary">Pin Hole</button>
                    <button id="oldbootbtn" class="btn btn-primary">Old Boot</button>
                    <button id="glowingsunbtn" class="btn btn-primary">Glow Sun</button>
                </nav>

                <nav class="filters">
                    <button id="embossbtn" class="btn btn-primary">Emboss</button>
                    <button id="crossprocessbtn" class="btn btn-primary">Cross Process</button>
                    <button id="pleasantbtn" class="btn btn-warning">Pleasant</button>
                    <button id="sunrisebtn" class="btn btn-primary">Sun Rise</button>
                    <button id="lomobtn" class="btn btn-primary">Lomo</button>
                    <button id="hdrbtn" class="btn btn-warning">HDR Effect</button>
                    <button id="oldpaperbtn" class="btn btn-warning">Old Paper</button>
                </nav>
            </div>
        </div>
    </div>
@endsection
