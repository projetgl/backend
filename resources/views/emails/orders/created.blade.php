@component('mail::message')
# Carte postale prête !

Votre carte postale est prête à être envoyée !

@component('mail::button', ['url' => $postcard->getPdfPublicUrl()])
Voir la carte postale
@endcomponent

Merci,<br>
{{ config('app.name') }}
@endcomponent
