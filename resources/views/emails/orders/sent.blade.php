@component('mail::message')
# Carte postale envoyée !

Votre carte postale est a été envoyée à son destinataire !

@component('mail::button', ['url' => $postcard->getPdfPublicUrl()])
Voir la carte postale
@endcomponent

Merci,<br>
{{ config('app.name') }}
@endcomponent
