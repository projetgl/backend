@component('mail::message')
# Carte postale reçue !

Votre carte postale est bien arrivée à son destinataire !

@component('mail::button', ['url' => $postcard->getPdfPublicUrl()])
Voir la carte postale
@endcomponent

Merci,<br>
{{ config('app.name') }}
@endcomponent
