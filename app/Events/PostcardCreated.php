<?php

namespace App\Events;

use App\Postcard;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PostcardCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $postcard;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Postcard $postcard)
    {
        $this->postcard = $postcard;
    }
}
