<?php

namespace App\Events;

use App\Picture;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PictureUploaded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $picture;

    /**
     * Create a new event instance.
     *
     * @param Picture $picture
     */
    public function __construct(Picture $picture)
    {
        $this->picture = $picture;
    }
}
