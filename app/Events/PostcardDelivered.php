<?php

namespace App\Events;

use App\Postcard;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PostcardDelivered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $postcard;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Postcard $postcard)
    {
        $this->postcard = $postcard;
    }
}
