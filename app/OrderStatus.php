<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $fillable = ['label'];
    protected $primaryKey = 'label';
    protected $keyType = 'string';

    public function orders()
    {
        return $this->hasMany(Postcard::class, 'status', 'label');
    }
}
