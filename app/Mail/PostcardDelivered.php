<?php

namespace App\Mail;

use App\Postcard;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PostcardDelivered extends Mailable
{
    use Queueable, SerializesModels;

    protected $postcard;

    /**
     * Create a new message instance.
     *
     * @param Postcard $postcard
     */
    public function __construct(Postcard $postcard)
    {
        $this->postcard = $postcard;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Carte postale arrivée')
            ->markdown('emails.orders.delivered', ['postcard' => $this->postcard]);
    }
}
