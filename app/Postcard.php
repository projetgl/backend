<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Storage;

/**
 * @property User $owner
 * @property OrderStatus $status
 * @property int $id
 * @property mixed $price
 */
class Postcard extends Model
{
    protected $fillable = [
        'body',
        'recipient',
        'address_line1',
        'address_line2',
        'zipcode',
        'city',
        'country',
        'price',
    ];

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    public function status() : BelongsTo
    {
        return $this->belongsTo(OrderStatus::class, 'status', 'label');
    }

    public function getFullname(): string
    {
        return '/postcards/' . $this->id . '.jpg';
    }

    public function getPublicUrl(): string
    {
        if (!Storage::disk('public')->exists($this->getFullname())) {
            return '';
        }

        return Storage::disk('public')->url($this->getFullname());
    }

    public function getPdfFullname(): string
    {
        return '/postcards/' . $this->id . '.pdf';
    }

    public function getPdfPublicUrl(): string
    {
        if (!Storage::disk('public')->exists($this->getPdfFullname())) {
            return '';
        }

        return Storage::disk('public')->url($this->getPdfFullname());
    }

    public function downloadPdf()
    {
        return Storage::disk('public')->download($this->getPdfFullname());
    }
}
