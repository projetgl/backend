<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property mixed $pictures
 */
class Tag extends Model
{
    protected $fillable = ['label',];

    public function pictures()
    {
        return $this->belongsToMany(
            Picture::class,
            'pictures_tags',
            'tag_id',
            'picture_id'
        );
    }
}
