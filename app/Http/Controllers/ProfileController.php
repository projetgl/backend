<?php

namespace App\Http\Controllers;

use App\User;
use function base64_decode;
use function dd;
use Exception;
use Illuminate\Http\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use function redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();

        $profile_picture = null;

        if (Storage::disk('public')->exists('/avatars/' . $user->id . '.jpg')) {
            $profile_picture = Storage::disk('public')->url('/avatars/' . $user->id . '.jpg');
        }

        return view('profile', [
            'user' => $user,
            'profile_picture' => $profile_picture,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    public function update(Request $request)
    {
        $validator = $this->validateProfile($request);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = Auth::user();

        $user->update($validator->validate());

        if ($request->has('profile_picture')) {
            Storage::disk('public')
                ->put(
                    'avatars/' . Auth::user()->id . '.jpg',
                    (string) Image::make($request->file('profile_picture'))->encode('jpg', 80)
                );
            $user->profile_picture = Storage::disk('public')->url('avatars/' . Auth::user()->id . '.jpg');
        }

        $user->save();

        return redirect($request->url())->with('status', 'Profil mis à jour !');
    }

    /**
     * Delete the current user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws Exception
     */
    public function delete()
    {
        Auth::user()->delete();

        return redirect('/');
    }

    /**
     * Updates the current user's password
     * @param Request $request
     * @return JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function updatePassword(Request $request)
    {
        $validator = $this->validatePassword($request);

        if ($validator->fails()) {
            return Response::json([
                'errors' => $validator->errors(),
            ], 400);
        }

        $user = Auth::user();

        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect($request->url())->with('status', 'Mot de passe mis à jour !');
    }

    /**
     * @SWG\Get(
     *   path="/user/profile",
     *   summary="Get the logged-in user's profile",
     *   operationId="userProfile",
     *   @SWG\Parameter(
     *     ref="#/parameters/access_token"
     *   ),
     *   @SWG\Response(response=200, description="The logged-in user's profile"),
     *   @SWG\Response(response=403, description="The user isn't logged in (missing access_token?)")
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiGetProfile(Request $request): JsonResponse
    {
        $this->authWithToken($request['access_token']);

        if (Auth::guest()) {
            return Response::json([], 403);
        }

        $user = Auth::user();

        $profile_picture = null;

        return Response::json($user);
    }

    /**
     * @SWG\Post(
     *   path="/user/profile",
     *   summary="Update the logged-in user's profile",
     *   operationId="userUpdateProfile",
     *   @SWG\Parameter(
     *     ref="#/parameters/access_token"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="User's email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="first_name",
     *     in="formData",
     *     description="User's first name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="last_name",
     *     in="formData",
     *     description="User's last name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address_line1",
     *     in="formData",
     *     description="User's address (first line)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address_line2",
     *     in="formData",
     *     description="User's address (second line)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     description="User's zipcode",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     description="User's city",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="country",
     *     in="formData",
     *     description="User's country",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="profile_picture",
     *     in="formData",
     *     description="The user's profile picture",
     *     type="file",
     *     format="binary"
     *   ),
     *   @SWG\Response(response=200, description="The updated profile"),
     *   @SWG\Response(response=400, description="Some parameters are invalid"),
     *   @SWG\Response(response=403, description="The user isn't logged in (missing access_token?)")
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function apiUpdateProfile(Request $request): JsonResponse
    {
        $this->authWithToken($request['access_token']);

        $validator = $this->apiValidateProfile($request);

        if ($validator->fails()) {
            Log::error($validator->errors());
            Log::debug($request->all());
            return Response::json([
                'errors' => $validator->errors(),
            ], 400);
        }

        $user = Auth::user();

        $user->update($validator->validate());

        if ($request->profile_picture !== null && $request->has('profile_picture')) {
            $picture = base64_decode($request->profile_picture);
            Storage::disk('public')
                ->put(
                    'avatars/' . Auth::user()->id . '.jpg',
                    (string) Image::make($picture)->encode('jpg', 80)
                );
            $user->profile_picture = Storage::disk('public')->url('avatars/' . Auth::user()->id . '.jpg');
        }

        $user->save();

        return Response::json($user);
    }

    /**
     * @SWG\Post(
     *   path="/user/password",
     *   summary="Update the logged-in user's password",
     *   operationId="userUpdatePassword",
     *   @SWG\Parameter(
     *     ref="#/parameters/access_token"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="User's password",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password_confirmation",
     *     in="formData",
     *     description="User's password repeated",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Password updated"),
     *   @SWG\Response(response=400, description="Some parameters are invalid"),
     *   @SWG\Response(response=403, description="The user isn't logged in (missing access_token?)")
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiUpdatePassword(Request $request): JsonResponse
    {
        $this->authWithToken($request['access_token']);

        $validator = $this->validatePassword($request);

        if ($validator->fails()) {
            return Response::json([
                'errors' => $validator->errors(),
            ], 400);
        }

        $user = Auth::user();

        $user->password = bcrypt($request->get('password'));
        $user->save();

        return Response::json(['success' => true]);
    }

    /**
     * @SWG\Delete(
     *   path="/user/profile",
     *   summary="Delete the logged-in user's profile",
     *   operationId="userDeleteProfile",
     *   @SWG\Parameter(
     *     ref="#/parameters/access_token"
     *   ),
     *   @SWG\Response(response=200, description="User deleted"),
     *   @SWG\Response(response=403, description="The user isn't logged in (missing access_token?)")
     * )
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function apiDeleteProfile(Request $request)
    {
        $this->authWithToken($request['access_token']);

        Auth::user()->delete();

        return Response::json(['success' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Validation\Validator
     */
    protected function validateProfile(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
            'profile_picture' => 'nullable|sometimes|image',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Validation\Validator
     */
    protected function apiValidateProfile(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
            'profile_picture' => 'nullable|sometimes|base64',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Validation\Validator
     */
    protected function validatePassword(Request $request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
}
