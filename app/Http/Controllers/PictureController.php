<?php

namespace App\Http\Controllers;

use App\Events\PictureUploaded;
use App\Events\PostcardCreated;
use App\OrderStatus;
use App\Picture;
use App\Postcard;
use App\Tag;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Image;
use Response;
use Stripe\Charge;
use Stripe\Stripe;
use Validator;
use function base64_decode;

class PictureController extends Controller
{
    // Shared methods between API and frontend
    protected function get(Picture $picture)
    {
        return [
            'picture' => $picture,
            'picture_url' => $picture->getPublicUrl(),
            'tags' => $picture->tags,
        ];
    }

    protected function edit(Picture $picture, array $tags)
    {
        $picture->price += 200;
        $picture->tags()->detach();
        $newTags = [];

        foreach ($tags as $tag) {
            $newTags[] = Tag::firstOrCreate(['label' => $tag['name']])->id;
        }

        $picture->tags()->attach($newTags);
        $picture->save();

        return $picture;
    }

    protected function validatePicture($data)
    {
        return Validator::make($data, [
            'title' => 'required|string',
            'description' => 'nullable|string',
            'picture' => 'required|image',
            'price' => 'nullable|integer|min:0',
        ]);
    }

    protected function apiValidatePicture($data)
    {
        return Validator::make($data, [
            'title' => 'required|string',
            'description' => 'nullable|string',
            'picture' => 'required|base64',
            'price' => 'nullable|integer|min:0',
        ]);
    }

    /**
     * @param $title
     * @param $description
     * @param $price
     * @param $file
     * @param $lat
     * @param $lng
     * @return Picture
     */
    protected function add($title, $description, $price, $file, $lat, $lng)
    {
        $picture = new Picture([
            'title' => $title,
            'description' => $description ?? '',
            'price' => ($price ?? 0) + 200,
            'is_nsfw' => false,
            'lat' => $lat,
            'lng' => $lng,
        ]);

        $picture->owner()->associate(Auth::user());

        $picture->save();

        $image = Image::make($file)->fit(840, 596, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg', 80);

        Storage::disk('public')
            ->put('pictures/' . $picture->id . '.jpg', (string)$image);

        event(new PictureUploaded($picture));

        return $picture;
    }

    protected function search($tag)
    {
        try {
            $pictures = Tag::where('label', $tag)->firstOrFail()->pictures()->get();
        } catch (ModelNotFoundException $e) {
            $pictures = [];
        }

        return [
            'pictures' => $pictures,
            'results' => $pictures !== [] ? $pictures->count() : 0,
            'tag' => $tag,
        ];
    }

    // Common methods
    public function download(Picture $picture)
    {
        if (!Storage::disk('public')->exists('/pictures/' . $picture->id . '.jpg')) {
            return abort(404);
        }

        return Storage::disk('public')->download('/pictures/' . $picture->id . '.jpg');
    }

    // Frontend methods
    public function frontShow(Picture $picture)
    {
        return view('pictures.show', $this->get($picture));
    }

    public function frontShowAdd()
    {
        return view('pictures.add');
    }

    public function frontAdd(Request $request)
    {
        $validator = $this->validatePicture($request->all());

        try {
            $validator->validate();
        } catch (ValidationException $e) {
            return back()->withErrors($validator)->withInput();
        }

        $picture = $this->add($request->title, $request->description, $request->price, $request->file('picture'),
            $request->lat, $request->lng);

        return redirect()->route('showPicture', [$picture]);
    }

    public function frontShowEdit(Picture $picture)
    {
        return view('pictures.edit', [
            'picture' => $picture,
            'tags' => $picture->tags()->get()->implode('label', ';'),
        ]);
    }

    public function frontEdit(Request $request, Picture $picture)
    {
        $picture->tags()->detach();

        foreach (explode(';', $request->tags) as $tag) {
            $tags[] = Tag::firstOrCreate(['label' => $tag])->id;
        }

        $picture->tags()->attach($tags);

        $picture->update([
            'description' => $request->description,
            'price' => $request->price,
        ]);

        $picture->save();

        return redirect()->route('showPicture', $picture);
    }

    public function frontSearch(Request $request)
    {
        return view('pictures.search', $this->search($request->tag));
    }

    public function frontDelete(Picture $picture)
    {
        if (!Auth::user()->can('delete', $picture)) {
            abort(403);
        }

        $picture->delete();
        return redirect('/');
    }

    public function frontShowOrder(Picture $picture)
    {
        return view('pictures.order', $this->get($picture));
    }

    public function frontOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'body' => 'required|string',
            'recipient' => 'required|string',
            'address_line1' => 'required|string',
            'address_line2' => 'nullable|string',
            'zipcode' => 'required|integer',
            'city' => 'required|string',
            'country' => 'required|string',
            'image' => 'required|base64image',
            'stripe_token' => 'required',
            'picture_id' => 'required|integer',
        ]);

        try {
            $data = $validator->validate();
        } catch (ValidationException $e) {
            return back()->withErrors($validator)->withInput();
        }

        $price = (new Picture)->find($request->picture_id)->price;

        Stripe::setApiKey(config('services.stripe.secret'));

        $charge = Charge::create(array(
            'amount' => $price,
            'currency' => 'eur',
            'description' => 'Carte postale',
            'source' => $request->stripe_token,
        ));

        if (!$charge['paid']) {
            return back()->withErrors(['message' => 'Paiement refusé']);
        }

        $postcard = new Postcard($data);
        $postcard->owner_id = Auth::id();
        $postcard->status = OrderStatus::find('En préparation')->label;
        $postcard->price = $price;
        $postcard->save();

        $image = Image::make(base64_decode(explode(',', $request->image)[1]))
            ->fit(420, 298, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 80);

        Storage::disk('public')
            ->put('postcards/' . $postcard->id . '.jpg', (string)$image);

        event(new PostcardCreated($postcard));

        return redirect()->route('showOrder', $postcard);
    }

    public function frontLike(Picture $picture)
    {
        $picture->like();

        return back();
    }

    public function frontDislike(Picture $picture)
    {
        $picture->dislike();

        return back();
    }

    // API methods

    /**
     * @SWG\Get(
     *   path="/picture/{pictureId}",
     *   summary="Gets the picture's informations",
     *   operationId="pictureGet",
     *   @SWG\Parameter(
     *     name="pictureId",
     *     in="path",
     *     description="Picture's ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="The picture's information"),
     *   @SWG\Response(response=404, description="{pictureId} doesn't exist")
     * )
     * @param Picture $picture
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiGet(Picture $picture): \Illuminate\Http\JsonResponse
    {
        return Response::json($this->get($picture));
    }

    // TODO: Write Swagger docs for pic creating through API
    public function apiAdd(Request $request)
    {
        $this->authWithToken($request->access_token);

        if (!Auth::user()->can('create', Picture::class)) {
            return Response::json([], 403);
        }

        // TODO: Pic creation through API
        // Remember to decode_base64 before sending $file to add()
        abort(501);
        return Response::json([]);
    }

    // TODO: Write Swagger docs for pic editing through API
    public function apiEdit(Request $request, Picture $picture)
    {
        $this->authWithToken($request->access_token);

        if (!Auth::user()->can('update', $picture)) {
            return Response::json([], 403);
        }

        // TODO: Pic editing through API
        abort(501);
        return Response::json([]);
    }

    // TODO: Write Swagger docs for pic deleting through API
    public function apiDelete(Request $request, Picture $picture)
    {
        $this->authWithToken($request->access_token);

        if (!Auth::user()->can('delete', $picture)) {
            return Response::json([], 403);
        }

        $picture->delete();
        return Response::json([
            'status' => 'deleted',
        ]);
    }

    /**
     * @SWG\Get(
     *   path="/picture/search?tag={tag}",
     *   summary="Searches for {tag}",
     *   operationId="pictureSearch",
     *   @SWG\Parameter(
     *     name="tag",
     *     in="query",
     *     description="Searched-for tag",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Pictures having {tag}")
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiSearch(Request $request)
    {
        return Response::json($this->search($request->tag));
    }
}
