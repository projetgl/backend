<?php

/**
 * @SWG\Swagger(
 *   basePath="/api",
 *   @SWG\Info(
 *     title="iSend API",
 *     version="1.0.0"
 *   ),
 *   @SWG\Response(response=500, description="internal server error")
 * )
 *
 * @SWG\Parameter(
 *   name="access_token",
 *   in="query",
 *   description="User's access token",
 *   required=true,
 *   type="string"
 * )
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use function var_dump;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function authWithToken($accessToken)
    {
        if (empty($accessToken)) {
            return;
        }

        try {
            $user = User::where(['access_token' => $accessToken])->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return;
        }

        Auth::login($user);
    }
}
