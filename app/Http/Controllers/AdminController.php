<?php

namespace App\Http\Controllers;

use App\Events\PostcardDelivered;
use App\Events\PostcardSent;
use App\OrderStatus;
use App\Postcard;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function frontShowIndex()
    {
        return view('admin.index', [
            'users' => [
                'confirmed' => User::where(['confirmed' => 1])->count(),
                'notConfirmed' => User::where(['confirmed' => 0])->count(),
                'total' => User::count(),
            ],
            'orders' => [
                'pending' => Postcard::where(['status' => 'En préparation'])->count(),
                'sent' => Postcard::where(['status' => 'Expédié'])->count(),
                'delivered' => Postcard::where(['status' => 'Reçu'])->count(),
                'total' => Postcard::count(),
            ],
        ]);
    }

    public function frontShowUsers()
    {
        return view('admin.users', [
            'users' => User::all(),
        ]);
    }

    public function frontShowAllOrders()
    {
        return view('admin.orders', [
            'orders' => Postcard::all(),
        ]);
    }

    public function frontShowOrder(Postcard $postcard)
    {
        return view('admin.show', [
            'order' => $postcard,
        ]);
    }

    public function frontUpdateOrder(Postcard $postcard, Request $request)
    {
        if ($request->status === 'Expédié') {
            $postcard->status()->dissociate();
            $postcard->status()->associate(OrderStatus::find($request->status));
            $postcard->save();
            event(new PostcardSent(Postcard::find($postcard)->first()));
        }

        if ($request->status === 'Reçu') {
            $postcard->status()->dissociate();
            $postcard->status()->associate(OrderStatus::find($request->status));
            $postcard->save();
            event(new PostcardDelivered(Postcard::find($postcard)->first()));
        }

        return redirect()->route('adminAllOrders');
    }
}
