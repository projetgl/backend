<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class LoginController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/login",
     *   summary="Login as a user",
     *   operationId="login",
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="User's email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="User's password",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful login"),
     *   @SWG\Response(response=403, description="bad login/password")
     * )
     *
     */

    public function login(Request $request) {
        $data = $request->all();
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            return \Response::json([
                'access_token' => User::where('email', $data['email'])->first()->access_token,
            ], 200);
            // TODO: Only get if confirmed=1
        }

        return \Response::json([
            'access_token' => null,
        ], 403);
    }
}
