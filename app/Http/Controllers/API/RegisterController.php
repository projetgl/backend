<?php

namespace App\Http\Controllers\API;

use function date_get_last_errors;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Bestmomo\LaravelEmailConfirmation\Traits\RegistersUsers;

class RegisterController extends \App\Http\Controllers\Auth\RegisterController
{
    use RegistersUsers;

    /**
     * @SWG\Post(
     *   path="/register",
     *   summary="Register a new user",
     *   operationId="register",
     *   @SWG\Parameter(
     *     name="nickname",
     *     in="formData",
     *     description="User's username",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="User's password",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password_confirmation",
     *     in="formData",
     *     description="User's password repeated",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="User's email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="first_name",
     *     in="formData",
     *     description="User's first name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="last_name",
     *     in="formData",
     *     description="User's last name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address_line1",
     *     in="formData",
     *     description="User's address (first line)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address_line2",
     *     in="formData",
     *     description="User's address (second line)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     description="User's zipcode",
     *     required=false,
     *     type="number"
     *   ),
     *   @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     description="User's city",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="country",
     *     in="formData",
     *     description="User's country",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="user created"),
     *   @SWG\Response(response=400, description="couldn't create user, check error in response")
     * )
     */

    public function new(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json([
                'success' => false,
                'errors' => $validator->errors(),
            ], 400);
        }

        $user = $this->create($request->all());
        $user->confirmation_code = str_random(30);
        $user->save();

        event(new Registered($user));

        $this->notifyUser($user);

        return Response::json(['success' => true], 200);
//        if (!$this->register($request)) {
//            return Response::json(['success' => false], 422);
//        }
//
//        return Response::json(['success' => true], 200);
    }
}
