<?php

namespace App\Http\Controllers;

use App;
use App\Postcard;
use App\User;
use Auth;

class OrdersController extends Controller
{
    public function frontShowOrders()
    {
        return view('orders.list', [
            'orders' => Auth::user()->postcards()->with('status')->get()
        ]);
    }

    public function frontShow(Postcard $postcard)
    {
        return view('orders.show', [
            'order' => $postcard,
            'user' => Auth::user(),
        ]);
    }

    public function frontShowBill(Postcard $postcard, User $user)
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('orders.pdfbill', [
            'order' => $postcard,
            'user' => Auth::user(),
        ]);

        return $pdf->download('facture.pdf');
    }
}
