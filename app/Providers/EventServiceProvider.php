<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\PictureUploaded' => [
            'App\Listeners\PictureUploadedListener',
        ],
        'App\Events\PostcardCreated' => [
            'App\Listeners\PostcardCreatedListener',
        ],
        'App\Events\PostcardSent' => [
            'App\Listeners\PostcardSentListener',
        ],
        'App\Events\PostcardDelivered' => [
            'App\Listeners\PostcardDeliveredListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
