<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        Validator::extend('base64', function ($attribute, $value, $parameters, $validator) {
            return preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $value);
        });

        Validator::extend('base64image', function ($attribute, $value, $parameters, $validator) {
            $explode = explode(',', $value);
            $allow = ['png', 'jpg', 'svg', 'gif'];
            $format = str_replace(
                [
                    'data:image/',
                    ';',
                    'base64',
                ],
                '',
                $explode[0]
            );

            if (!in_array($format, $allow)) {
                return false;
            }

            return preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $explode[1]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
