<?php

namespace App;

use App\Like;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;

/**
 * @property User $owner
 * @property Tag[] $tags
 * @property int $id
 * @property int $owner_id
 * @property int price
 * @property int $score
 * @property mixed $likes
 * @property boolean $is_nsfw
 * @property float $lat
 * @property float $lng
 */
class Picture extends Model
{
    protected $fillable = [
        'lat',
        'lng',
        'title',
        'description',
        'is_nsfw',
        'price',
    ];

    /**
     * Returns the owner's model
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     * Returns all tags related to the picture
     *
     * @return BelongsToMany
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(
            Tag::class,
            'pictures_tags',
            'picture_id',
            'tag_id'
        );
    }

    /**
     * Returns the file full name
     *
     * @return string
     */
    public function getFullName(): string
    {
        return '/pictures/' . $this->id . '.jpg';
    }

    /**
     * Returns the publicly accessible URL to the file.
     *
     * @return string
     */
    public function getPublicUrl(): string
    {
        if (!Storage::disk('public')->exists($this->getFullName())) {
            return '';
        }

        return Storage::disk('public')->url($this->getFullName());
    }

    public function likes()
    {
        return $this->belongsToMany(
            User::class,
            'likes',
            'picture_id',
            'user_id'
        )->withPivot('score');
    }

    public function getScoreAttribute()
    {
        return $this->likes()->get()->reduce(function ($carry, $item) {
            return $carry + $item->pivot->score;
        }, 0);
    }

    public function like()
    {
        $this->likes()->detach(Auth::user());
        $this->likes()->save(Auth::user(), ['score' => 1]);

        return $this;
    }

    public function dislike()
    {
        $this->likes()->detach(Auth::user());
        $this->likes()->save(Auth::user(), ['score' => -1]);

        return $this;
    }
}
