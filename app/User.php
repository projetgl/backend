<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

/**
 * @property string $profile_picture
 * @property string $password
 * @property mixed $is_admin
 * @property mixed $id
 * @property mixed $postcards
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $attributes = ['access_token',];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['access_token',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'confirmed',
        'confirmation_code',
        'access_token',
    ];

    public function __construct(array $attributes = [])
    {
        $this->setRawAttributes([
            'access_token' => (string) Str::uuid(),
        ], true);
        parent::__construct($attributes);
    }

    public function likes()
    {
        return $this->belongsToMany(
            Picture::class,
            'likes',
            'picture_id',
            'user_id'
        );
    }

    public function postcards()
    {
        return $this->hasMany(Postcard::class, 'owner_id', 'id');
    }
}
