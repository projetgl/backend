<?php

namespace App\Policies;

use App\User;
use App\Picture;
use Illuminate\Auth\Access\HandlesAuthorization;
use Auth;

class PicturePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->is_admin) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the picture.
     *
     * @param  \App\User  $user
     * @param  \App\Picture  $picture
     * @return mixed
     */
    public function view(User $user, Picture $picture)
    {
        return true;
    }

    /**
     * Determine whether the user can create pictures.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return Auth::check();
    }

    /**
     * Determine whether the user can update the picture.
     *
     * @param  \App\User  $user
     * @param  \App\Picture  $picture
     * @return mixed
     */
    public function update(User $user, Picture $picture)
    {
        return $user->id === $picture->owner_id;
    }

    /**
     * Determine whether the user can delete the picture.
     *
     * @param  \App\User  $user
     * @param  \App\Picture  $picture
     * @return mixed
     */
    public function delete(User $user, Picture $picture)
    {
        return $user->id === $picture->owner_id;
    }

    /**
     * Determine whether the user can like the picture.
     *
     * @return bool
     */
    public function like()
    {
        return Auth::check();
    }

    /**
     * Determine whether the user can order the picture.
     *
     * @return bool
     */
    public function order()
    {
        return Auth::check();
    }
}
