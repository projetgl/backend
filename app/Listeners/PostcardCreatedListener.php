<?php

namespace App\Listeners;

use App;
use App\Events\PostcardCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Storage;

class PostcardCreatedListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  PostcardCreated $event
     * @return void
     * @throws \Throwable
     */
    public function handle(PostcardCreated $event)
    {
        $postcard = $event->postcard;

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML(view('pdfpostcard', ['postcard' => $postcard])->render());

        Storage::disk('public')
            ->put('postcards/' . $postcard->id . '.pdf', $pdf->output());

        Mail::to($postcard->owner->email)
            ->send(new App\Mail\PostcardReady($postcard));
    }
}
