<?php

namespace App\Listeners;

use App;
use App\Events\PostcardSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class PostcardSentListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param PostcardSent $event
     * @return void
     * @throws \Throwable
     */
    public function handle(PostcardSent $event)
    {
        $postcard = $event->postcard;

        Mail::to($postcard->owner->email)
            ->send(new App\Mail\PostcardSent($postcard));
    }
}
