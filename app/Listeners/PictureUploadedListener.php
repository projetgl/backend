<?php

namespace App\Listeners;

use App\Events\PictureUploaded;
use App\Tag;
use GuzzleHttp;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use function config;
use function json_decode;

class PictureUploadedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PictureUploaded $event
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(PictureUploaded $event): void
    {
        $picture = $event->picture;
        $picFile = Storage::disk('public')->get($picture->getFullName());
        $client = new GuzzleHttp\Client();

        $tags = [];

        $res = json_decode($client->post(
            'https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/analyze?visualFeatures=Tags,Adult',
            [
                'headers' => [
                    'Content-Type' => 'application/octet-stream',
                    'Ocp-Apim-Subscription-Key' => config('services.msvision.key'),
                ],
                'body' => $picFile,
            ]
        )->getBody(), true);

        foreach ($res['tags'] as $tag) {
            if ($tag['confidence'] > 0.5) {
                $tags[] = Tag::firstOrCreate(['label' => $tag['name']])->id;
            }
        }

        $picture->is_nsfw = $res['adult']['isAdultContent'];

        if ($picture->lat !== 0.0 && $picture->lng !== 0.0) {
            $res = json_decode($client->get(
                'https://maps.googleapis.com/maps/api/geocode/json?latlng='
                . $picture->lat . ',' . $picture->lng
                . '&key=' . config('services.gmaps.key')
                . '&result_type=locality'
            )->getBody(), true);

            // Adds the city to tags
            $tags[] = Tag::firstOrCreate([
                'label' => $res['results'][0]['address_components'][0]['long_name']
            ])->id;
        }

        $picture->tags()->attach($tags);

        $picture->save();
    }
}
