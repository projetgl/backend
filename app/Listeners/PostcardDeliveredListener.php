<?php

namespace App\Listeners;

use App;
use App\Events\PostcardDelivered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class PostcardDeliveredListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param PostcardDelivered $event
     * @return void
     * @throws \Throwable
     */
    public function handle(PostcardDelivered $event)
    {
        $postcard = $event->postcard;

        Mail::to($postcard->owner->email)
            ->send(new App\Mail\PostcardDelivered($postcard));
    }
}
