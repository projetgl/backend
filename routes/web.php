<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Static views
Route::view('/', 'welcome')->name('index');
Route::get('/home', function () {
    return redirect('/');
});
Route::view('/about', 'about')->name('about');

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/profile', 'ProfileController@show')->name('profile');
    Route::put('/profile', 'ProfileController@update')->name('updateProfile');
    Route::post('/profile', 'ProfileController@updatePassword')->name('updatePassword');
    Route::delete('/profile', 'ProfileController@delete')->name('deleteProfile');

    Route::get('/profile/orders', 'OrdersController@frontShowOrders')->name('showAllOrders');
    Route::get('/profile/orders/{postcard}', 'OrdersController@frontShow')->name('showOrder');
    Route::get('/profile/orders/{postcard}/bill', 'OrdersController@frontShowBill')->name('showOrderBill');
});

Route::middleware('admin')->prefix('admin')->group(function() {
    Route::get('', 'AdminController@frontShowIndex')->name('adminIndex');
    Route::get('/users', 'AdminController@frontShowUsers')->name('adminShowUsers');
    Route::get('/orders', 'AdminController@frontShowAllOrders')->name('adminAllOrders');
    Route::get('/orders/{postcard}', 'AdminController@frontShowOrder')->name('adminShowOrder');
    Route::put('/orders/{postcard}', 'AdminController@frontUpdateOrder')->name('adminUpdateOrder');
});

Route::get('/picture/search', 'PictureController@frontSearch')->name('searchPicture');
Route::middleware('auth')->group(function () {
    Route::get('/picture/add', 'PictureController@frontShowAdd')->name('showAddPicture');
    Route::post('/picture/add', 'PictureController@frontAdd')->name('addPicture');
    Route::post('/picture/order', 'PictureController@frontOrder')->name('orderPicture');
});

Route::prefix('/picture')->group(function () {
    Route::get('/{picture}', 'PictureController@frontShow')->name('showPicture');
    Route::get('/{picture}/download', 'PictureController@download')->name('downloadPicture');
    Route::middleware('auth')->group(function () {
        Route::get('/{picture}/edit', 'PictureController@frontShowEdit')->name('showEditPicture');
        Route::put('/{picture}', 'PictureController@frontEdit')->name('editPicture');
        Route::delete('/{picture}', 'PictureController@frontDelete')->name('deletePicture');
        Route::post('/{picture}/like', 'PictureController@frontLike')->name('likePicture');
        Route::post('/{picture}/dislike', 'PictureController@frontDislike')->name('dislikePicture');
        Route::get('/{picture}/order', 'PictureController@frontShowOrder')->name('showOrderPicture');
    });
});
