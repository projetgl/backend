<?php

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login','API\LoginController@login');
Route::post('register', 'API\RegisterController@new');

Route::get('/user/profile', 'ProfileController@apiGetProfile');

// Verb Should be PUT, but PHP only accepts file uploads with a POST verb
// (see: https://github.com/laravel/framework/issues/13457)
Route::post('/user/profile', 'ProfileController@apiUpdateProfile');
Route::delete('/user/profile', 'ProfileController@apiDeleteProfile');
Route::post('/user/password', 'ProfileController@apiUpdatePassword');

Route::prefix('picture')->group(function () {
    Route::get('search', 'PictureController@apiSearch');
    Route::get('{picture}', 'PictureController@apiGet');
    Route::post('', 'PictureController@apiAdd');
    Route::put('{picture}', 'PictureController@apiEdit');
    Route::delete('{picture}', 'PictureController@apiDelete');

    Route::post('{picture}/like', 'PictureController@apiLike');
    Route::post('{picture}/dislike', 'PictureController@apiDislike');
});
